// CHARTILESET DATA...
// 37 tiles, 2x2 (4) cells per tile, 8 bits per cell, total size is 148 ($94) bytes.

//$fb/fc source $fd/fe target
//source will update automatically until $ff value is found
//target will automatically move to next char
chartileset_data:

.byte $00,$00,$00,$00,$01,$02,$03,$04,$05,$06,$07,$08,$09,$0A,$0B,$0C
.byte $0D,$0E,$07,$0F,$05,$10,$07,$11,$05,$10,$03,$12,$09,$0A,$0B,$13
.byte $14,$15,$03,$04,$16,$17,$18,$19,$00,$1A,$1B,$0F,$14,$1C,$03,$1D
.byte $1E,$00,$07,$1F,$20,$21,$03,$03,$22,$23,$03,$03,$09,$0E,$0B,$0F
.byte $05,$06,$03,$24,$09,$0E,$25,$26,$05,$06,$03,$27,$28,$29,$2A,$2B
.byte $2C,$2D,$2E,$2F,$1E,$1A,$0B,$0F,$1E,$1A,$30,$31,$1E,$1A,$32,$33
.byte $34,$35,$36,$37,$34,$35,$2E,$38,$39,$3A,$3B,$3C,$09,$02,$3D,$3E
.byte $3F,$40,$18,$19,$41,$06,$07,$42,$43,$06,$44,$08,$45,$15,$46,$04
.byte $05,$47,$48,$49,$01,$4A,$0B,$49,$39,$3A,$2E,$2F,$28,$06,$0B,$08
.byte $28,$02,$2A,$4B

