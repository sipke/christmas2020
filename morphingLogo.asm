.const MAX_DOTS = 20
morphLogo:
{
    inc delayIt
    lda delayIt
    cmp #5
    beq !+
    rts

!:
    lda #0
    sta delayIt
    //clear working area
    lda #0
    ldx #23
!:  sta $8000-128,x
    dex
    bpl !-    
    
loop:    
    ldx dotNr
    lda workAreaY,x 
    tay
    lda workAreaX,x 
    tax
    jsr plot
    lda dotNr
    clc
    adc #1
    sta dotNr
    cmp #MAX_DOTS
    bne loop
    lda #0
    sta dotNr
dotsDrawn:
//updateWorkArea--------------------
updateWorkArea:
    ldy curLetter
    lda timesTwenty,y
    tay
    ldx #0
loop2:  
    lda letterDefsX,y
    cmp workAreaX,x
    beq checkY
    bcs xGreater
    dec workAreaX,x
    jmp checkY
xGreater:    
    inc workAreaX,x
checkY:
    lda letterDefsY,y
    cmp workAreaY,x
    beq doLoop
    bcs yGreater
    dec workAreaY,x
    jmp doLoop
yGreater:    
    inc workAreaY,x
doLoop:
    iny
    inx
    cpx #20
    bne loop2
//check if all equal----------------------------
equalCheck:
    ldx curLetter
    lda timesTwenty,x
    tax
    ldy #0
loop4:  
    lda letterDefsX,x
    cmp workAreaX,y
    bne endLoop4
    lda letterDefsY,x
    cmp workAreaY,y
    bne endLoop4
    inx
    iny
    cpy #20
    bne loop4
    jmp copyNewDef
endLoop4:
    rts
copyNewDef:    
    //copy after set amount of equals
    inc equalCount
    lda equalCount
    cmp #10
    bne endLoop4
    lda #0
    sta equalCount
    //copy to workArea
    ldx curLetter
    lda timesTwenty,x
    tax
    ldy #0
loop3:  lda letterDefsX,x
    sta workAreaX,y
    lda letterDefsY,x
    sta workAreaY,y
    inx
    iny
    cpy #20
    bne loop3

    lda curLetter
    clc
    adc #$01
    and #$03
    sta curLetter
leave:    
    rts
plot:
    lda valForX,x
    sta sm + 1
    lda timesThree,y
    tay
    lda $8000-128,y
sm: ora #0
    sta $8000-128,y
    rts

valForX:
    .byte 128,64, 32,16,8,4,2,1   
timesThree:
    .byte 0,3,6,9,12,15,18,21
timesTwenty:
    .byte 0,20,40,60
curLetter:
    .byte 0        
dotNr:
    .byte 0
equalCount:
    .byte 0
delayIt:    
    .byte 0
letterDefsX:
    //F
    .byte 6,5,4,3,2,1,1,1,1,2,3,4,1,1,1,1,1,1,1,1
    //4
    .byte 5,5,6,5,5,5,5,4,3,2,1,2,3,4,4,4,4,4,4,5
    //C
    .byte 6,5,4,3,2,1,1,1,1,1,2,3,4,5,6,6,6,6,6,6
    //G
    .byte 5,6,6,5,4,3,2,1,1,1,1,1,2,3,4,5,6,6,6,6

letterDefsY:
    //F
    .byte 0,0,0,0,0,0,1,2,3,3,3,3,4,5,6,6,6,6,6,6
    //4
    .byte 6,5,4,3,2,1,0,4,4,4,4,3,2,1,1,1,1,1,1,4
    //C
    .byte 1,0,0,0,0,1,2,3,4,5,6,6,6,6,5,5,5,5,5,5
    //G
    .byte 4,4,5,6,6,6,6,5,4,3,2,1,0,0,0,0,1,1,1,1
workAreaX:
    .fill 20,0
workAreaY:
    .fill 20,0
}