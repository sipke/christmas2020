copySid:
{
    ldy #ceil(sid.size/256)
    ldx #0
loop:  
    lda sidLoad,x
    sta sid.location,x
    inx
    bne loop
    inc loop+2
    inc loop+5
    dey
    bne loop
}

    rts


setCharmem:
    ldx #0
!:  sta CHARS_MEM,x
    sta CHARS_MEM+250,x
    sta CHARS_MEM+500,x
    sta CHARS_MEM+750,x
    inx
    cpx #250
    bne !-
    rts
setD800:
    ldx #0
!:  sta $d800,x
    sta $d800+256,x
    sta $d800+512,x
    sta $d800+768,x
    inx
    bne !-
    rts
// bitmap 0 sprite setup    
spriteSetup:
    lda #0
    sta $d010

    lda #$54
    sta $d00a
    lda #$77
    sta $d00b
    lda #MOUTH_MASK_START
    sta $63fd

    lda #$8b
    sta $d00c
    lda #$53
    sta yPos5to7+1

    lda #SANTA_BALL_SPRITE_START
    sta $63fe

    lda #$6c
    sta $d00e
    lda #$75
    sta yPos5to7+2
    //sta $d00f

    lda #SANTA_BEARD_START
    sta $63ff


    lda #255
    sta $d015
    sta $d01c

    lda #LIGHT_GREY
    sta $d025
    lda #WHITE
    sta $d026

    ldx #4
    lda #LIGHT_RED
!:  sta $d027,x
    dex
    bpl !-

    lda #WHITE
    sta $d027+5
    lda #BLACK
    sta $d028+5
    sta $d029+5



    rts
relocateSprites:
{
    ldy #7
    ldx #0
loop:    
    lda spritesLoad,x
    sta SPRITE_MEM,x
    inx
    bne loop
    inc loop +2
    inc loop+5
    dey
    bne loop
    rts
}

relocateChars:
{
    ldy #8
    ldx #0
loop:    
    lda charsLoad,x
    sta CHAR_DOT_MEM,x
    inx
    bne loop
    inc loop +2
    inc loop+5
    dey
    bne loop
    rts
}
//X <irq, Y>irq, A irq line
setupIRQ:
    sei
    stx $fffe
    sty $ffff
    sta $d012
    lda $d011
    and #$7f
    sta $d011
    lda #$01
    sta $d01a
    lda #$7f
    sta $dc0d
    sta $dd0d
    lda $dc0d
    lda $dd0d
    lda #$35
    sta $01
    cli
    rts
relocateBitmap0:
{
    ldx #0
    ldy #40
loop:
    lda bitmap0OnLoad,x
    sta BITMAP0_10K_START,x
    inx 
    bne loop
    inc loop+2
    inc loop+5
    dey   
    bne loop
    rts
}
copyBitmap1:
{
    ldy #32
    ldx #0
loop:  
    lda BITMAP1_10K_START,x
    sta BITMAP_VIC_MEM,x
    inx
    bne loop
    inc loop +2
    inc loop +5
    dey
    bne loop
    ldx #0
!:  lda BITMAP1_10K_START+9000,x
    sta $d800,x
    lda BITMAP1_10K_START+9000+250,x
    sta $d800+250,x
    lda BITMAP1_10K_START+9000+500,x
    sta $d800+500,x
    lda BITMAP1_10K_START+9000+750,x
    sta $d800+750,x
    
    lda BITMAP1_10K_START+8000,x
    sta CHARS_MEM,x
    lda BITMAP1_10K_START+8000+250,x
    sta CHARS_MEM+250,x
    lda BITMAP1_10K_START+8000+500,x
    sta CHARS_MEM+500,x
    lda BITMAP1_10K_START+8000+750,x
    sta CHARS_MEM+750,x

    inx
    cpx #250 
    bne !-
}
    rts
copyCharRom:{
    sei
    lda #$33
    sta $01
    ldy #8
    ldx #0
loop:
    lda $d800,x
    sta CHAR_DOT_MEM,x
    inx
    bne loop
    inc loop+2
    inc loop+5
    dey
    bne loop
    lda #$35
    sta $01 
    cli
    //change @ to smiley
    ldx #7
!:  lda smiley,x
    sta CHAR_DOT_MEM,x
    dex
    bpl !-
    rts 
smiley:
    .byte %01111110    
    .byte %10000001    
    .byte %10100101    
    .byte %10000001    
    .byte %10100101    
    .byte %10011001    
    .byte %01000010    
    .byte %01111110    
}    
setupSpriteLastPart:
{
    ldx #127
    lda #0
!:  sta $8000-128,x
    dex
    bpl !- 
    lda #0
    sta $d01c   //no MC
    sta $d015
    lda #$20-2
    sta $d000
    lda #$30-2
    sta $d002
    lda #$e8-2
    sta $d003
    lda #$d0-2
    sta $d001

    lda #$20
    sta $d004
    lda #$30
    sta $d006
    lda #$e8
    sta $d007
    lda #$d0
    sta $d005

    lda #$f
    sta $d010
    //sta $d015
    lda #$ff
    sta $63f8
    sta $63fa
    lda #$fe
    sta $63f9
    sta $63fb
    lda #WHITE
    sta $d027
    sta $d028
    lda #BLACK
    sta $d029
    sta $d02a
    lda #$f
    sta $d01d
    lda #$a
    sta $d017
    rts
}
scroller:{
    lda fineScroll
    bne doStep
//filll buffer
    ldx curChar
    lda scrollerText,x
    tax
    lsr
    lsr
    lsr
    lsr
    lsr
    clc
    adc #>CHAR_DOT_MEM
    sta sm0+2
    txa
    and #%00011111
    asl
    asl
    asl
    sta sm0+1
    ldx #$07
sm0:
    lda $ffff,x
    sta buffer,x
    dex
    bpl sm0
    inc curChar
doStep:    
    .for(var i=0; i<8;i++){
        asl buffer+i
        rol $8000-64 + 26 +i*3
        rol $8000-64 + 25 +i*3
        rol $8000-64 + 24 +i*3
    }
    lda fineScroll
    clc
    adc #$01
    and #$07
    sta fineScroll
    rts

@curChar:
    .byte 0
fineScroll:
    .byte 0
@buffer:    
    .fill 8,0
.encoding "screencode_mixed"
scrollerText:    
    .text "A merry (if somewhat belated) christmas and a happy new year to all!"
    .text "  A special hello to my new buddies of F4CG!  Thanks for taking this "
    .text "stray one in @. I hope to be making great stuff with you guys in 2021!"
    .text "....... Goerp out ... see you next year!                              "
    // .text "      a MERRY (BELATED) CHRISTMAS AND A HAPPY NEW YEAR TO ALL!  "
    // .text "  aND A SPECIAL HELLO TO MY NEW BUDDIES OF f4cg!      tHANKS FOR"
    // .text " TAKING IN THIS STRAY ONE @! i HOPE TO BE MAKING GREAT STUFF WI"
    // .text "TH YOU GUYS IN 2021! gOERP OUT ..........................             "
}


copyBitmap0:
{
    ldy #32
    ldx #0
loop:  
    lda BITMAP0_10K_START,x
    sta BITMAP_VIC_MEM,x
    inx
    bne loop
    inc loop +2
    inc loop +5
    dey
    bne loop
    ldx #0
!:  lda BITMAP0_10K_START+9000,x
    sta $d800,x
    lda BITMAP0_10K_START+9000+250,x
    sta $d800+250,x
    lda BITMAP0_10K_START+9000+500,x
    sta $d800+500,x
    lda BITMAP0_10K_START+9000+750,x
    sta $d800+750,x
    
    lda BITMAP0_10K_START+8000,x
    sta CHARS_MEM,x
    lda BITMAP0_10K_START+8000+250,x
    sta CHARS_MEM+250,x
    lda BITMAP0_10K_START+8000+500,x
    sta CHARS_MEM+500,x
    lda BITMAP0_10K_START+8000+750,x
    sta CHARS_MEM+750,x

    inx
    cpx #250 
    bne !-


    rts
}
relocateBitmap1:
{
    ldx #0
    ldy #40
loop:
    lda bitmap1OnLoad,x
    sta BITMAP1_10K_START,x
    inx 
    bne loop
    inc loop+2
    inc loop+5
    dey   
    bne loop
    rts
}

getNextRandom:
    lda curRandom+1
    asl
    asl
    eor curRandom+1
    asl
    eor curRandom+1
    asl
    asl
    eor curRandom+1
    asl
    rol curRandom
    rol curRandom+1
    lda curRandom + 1
    rts