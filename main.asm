.const ANIMATION_DELAY = 6
// VIC MAP 
.const BITMAP_VIC_MEM = $4000
// $4000-$6000 bitmap
.const CHARS_MEM = $6000
// $6000-$6400 bitmap color data/char data
.const CHARS_MEM2 = $6400
// $6400-$6800 bitmap color data/char data for split screen text
.const CHAR_DOT_MEM = $6800
// $6800-$7000 charset data
.const SPRITE_MEM = $7000
// $7000-$8000 sprite data
.const BITMAP0_10K_START = $8000
.const BITMAP1_10K_START = BITMAP0_10K_START + 10*1024

.const NR_CORONA_SPRITES = 7
.const CORONA_SPRITE_START = (SPRITE_MEM & $3fff)/64
.const SANTA_BALL_SPRITE_START = CORONA_SPRITE_START + NR_CORONA_SPRITES
.const NR_SANTA_BALL_SPRITES = 7
.const SANTA_BEARD_START = SANTA_BALL_SPRITE_START + NR_SANTA_BALL_SPRITES
.const NR_BEARD_SPRITES = 3
.const MOUTH_MASK_START = SANTA_BEARD_START + NR_BEARD_SPRITES

.const MOUTH_MASK_X = $54
.const MOUTH_MASK_Y = $77

.const HO_HO_HO_X = $50
.const HO_HO_HO_Y = $38
.const HO_HO_HO_DELAY = 16

.import source "macros.asm"
.var sid = LoadSid("resources\hells_jingle.sid")

*=$0801 "basic start"
    BasicUpstart($080d)

*=$080d "MC start"
    lda #$36
    sta $01

    screenOff()
//    jsr $e544
  //relocate stuff
    jsr relocateBitmap1
    jsr relocateBitmap0
    jsr relocateChars
    jsr relocateSprites

*=* "part 0"
    setd018Chars(CHAR_DOT_MEM, CHARS_MEM)
    setVICBank(1)
    setMC()
    lda #GREEN
    sta $d020
    sta $d021

    lda #RED
    sta $d022
    lda #WHITE
    sta $d023

    lda #0
    jsr setCharmem

    lda #BLACK + 8
    jsr setD800 

    ldx #<testText
    stx $fb
    ldx #>testText
    stx $fc
    lda #<(CHARS_MEM + 83)
    ldy #>(CHARS_MEM + 83)

    jsr print

    sei
    lda #$35
    sta $01
    jsr copySid
    lda #0
    jsr sid.init

    ldx #<irq
    ldy #>irq
    lda #0
    jsr setupIRQ

    screenOn()

    lda #$ff
    sta lineCount
!:  lda lineCount
    bne !-

    // ldy #$80
    // jsr pause

    screenOff()
    jsr copyBitmap0 
    jsr spriteSetup

    setBitmap()
    setd018Bitmap($4000, $6000)
    lda #0
    sta $d021
    sta $d020
    inc part

    screenOn()

    lda #$ff
    sta lineCount
!:  lda lineCount
    bne !-
    lda #$ff
    sta lineCount
!:  lda lineCount
    bne !-
//     lda #$ff
//     sta lineCount
// !:  lda lineCount
//     bne !-

    inc part
    screenOff()

    lda #0
    sta $d015
    setd018Chars(CHAR_DOT_MEM, CHARS_MEM)

    lda #GREEN
    sta $d020
    sta $d021

    lda #RED
    sta $d022
    lda #WHITE
    sta $d023

    lda #0
    jsr setCharmem

    lda #BLACK + 8
    jsr setD800 

    ldx #<part3Text
    stx $fb
    ldx #>part3Text
    stx $fc
    lda #<(CHARS_MEM + 374)
    ldy #>(CHARS_MEM + 374)

    jsr print
    setTextMode()
    screenOn()
    lda #$cf
    sta lineCount
!:  lda lineCount
    bne !-

    inc part
    lda #GREEN
    sta $d020
    lda #BLACK
    sta $d021

    screenOff()
    jsr copyBitmap1
    lda #$00
    sta $d015

    setBitmap()
    setd018Bitmap($4000, $6000)
    screenOn()

    jsr copyCharRom
    jsr setupSpriteLastPart

    lda #$af
    sta lineCount
!:  lda lineCount
    bne !-

    lda #0
    ldx #127
!:  sta $8000-128,x
     dex
    bpl !-
    ldx #8
!:  sta buffer,x
    dex
    bpl !-

    sta curChar
    lda #$f
    sta $d015

    lda #$af
    sta lineCount
!:  lda lineCount
    bne !-

!:  lda $dc01
    cmp #239
    bne !-

    lda #$37
    sta $01
    jmp $fce2


    .import source "irq.asm"
    .import source "morphingLogo.asm"
    .import source "routines.asm"

print:
    printChars()
    rts

part3Text:
    textToBytes("so")
    .byte $ff
testText:
    textToBytes("        with half")
    .byte $fe
    textToBytes("       the world")
    .byte $fe
    textToBytes("      in lockdown")
    .byte $fe
    textToBytes("   the last thing")
    .byte $fe
    textToBytes("         we need")
    .byte $fe
    textToBytes("         is this")
    .byte $ff

part:
    .byte 0
animationDelay:
    .byte ANIMATION_DELAY
lineCount:
    .byte 0
hoFrame:
    .byte 0
hoFrameCounter:
    .byte HO_HO_HO_DELAY
hatFrame:
    .byte 0
hatFrameCounter:
    .byte 1
hatFrames:
    .var frL = List().add(3,4,5,6,6,5,4,3,2,1,0,0,1,2)
    .fill 14, SANTA_BALL_SPRITE_START + frL.get(i)
beardFrame:
    .byte 0
beardFrameCounter:
    .byte 1
beardFrames:
    .byte SANTA_BEARD_START,  SANTA_BEARD_START+1, SANTA_BEARD_START, SANTA_BEARD_START+2
animationCount:
    .byte 0
sidLoad:    
    .fill sid.size, sid.getData(i)
charsLoad:    
    .import binary "resources\Chrome (2x2, Subscii x37) - Chars.bin"
charTileDefs:    
    .import source "resources\Chrome (2x2, Subscii x37) - Tiles.asm"
.align 64
spritesLoad:
    .import binary "resources\coronas_small - Sprites.bin"
    .import binary "resources\santasball - Sprites.bin"
    .import binary "resources\santasbeard - Sprites.bin"
    .import binary "resources\santaMouthMaskPRJ_trimmed - Sprites.bin"    


.var sssanta = LoadBinary("resources\ssSanta4.kla")
bitmap0OnLoad:
    .fill sssanta.getSize()-2, sssanta.get(i+2)
.var ppsanta = LoadBinary("resources\porcupinesanta.kla")
bitmap1OnLoad:
    .fill ppsanta.getSize()-2, ppsanta.get(i+2)

