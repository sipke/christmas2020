//macro's

.macro screenOff(){
    lda $d011 
    and #%11100111
    sta $d011    
}
.macro screenOn(){
    lda $d011 
    ora #%00010000
    sta $d011    
}


.const charbase = " abcdefghijklmnopqrstuvwxyz0123456789"
.const tiledefs = List().add($00,$00,$00,$00,$01,$02,$03,$04,$05,$06,$07,$08,$09,$0A,$0B,$0C,
$0D,$0E,$07,$0F,$05,$10,$07,$11,$05,$10,$03,$12,$09,$0A,$0B,$13,
$14,$15,$03,$04,$16,$17,$18,$19,$00,$1A,$1B,$0F,$14,$1C,$03,$1D,
$1E,$00,$07,$1F,$20,$21,$03,$03,$22,$23,$03,$03,$09,$0E,$0B,$0F,
$05,$06,$03,$24,$09,$0E,$25,$26,$05,$06,$03,$27,$28,$29,$2A,$2B,
$2C,$2D,$2E,$2F,$1E,$1A,$0B,$0F,$1E,$1A,$30,$31,$1E,$1A,$32,$33,
$34,$35,$36,$37,$34,$35,$2E,$38,$39,$3A,$3B,$3C,$09,$02,$3D,$3E,
$3F,$40,$18,$19,$41,$06,$07,$42,$43,$06,$44,$08,$45,$15,$46,$04,
$05,$47,$48,$49,$01,$4A,$0B,$49,$39,$3A,$2E,$2F,$28,$06,$0B,$08,
$28,$02,$2A,$4B)

//A <print address
//Y >printa address
//$fb/fc source data
.macro printChars(){
    sta sm0+1
    sty sm0+2
    ldy #0
    ldx #0
loop:    
    lda ($fb),y
//     bne !+
//     lda sm0+1
//     clc
//     adc #1
//     sta sm0+1
//     lda sm0+2
//     adc #0
//     sta sm0+2
//     jmp next
// !:    
    cmp #$ff
    bne !+
    jmp done
!:  cmp #$fe
    bne !+
    lda sm0+1
    clc
    adc #40
    sta sm0+1
    lda sm0+2
    adc #0
    sta sm0+2
    ldx #$ff
    jmp next
!:  
sm0:
   sta $ffff,x  
next:
    inx
    iny
    bne !+
    inc $fc
!:    
    jmp loop

done:
    
}


.macro textToBytes(s) {
    .var c
    .var index
    //top row
    .for(var i=0;i<s.size(); i++) {
        .eval c=s.charAt(i)
        .eval index = getCharIndex(c)*4
        .if(index !=0) {
            .byte tiledefs.get(index), tiledefs.get(index+1)
        }else{
            .byte tiledefs.get(index)
        }
    }
    .byte $fe
    //bottom row
    .for(var i=0;i<s.size(); i++) {
        .eval c=s.charAt(i)
        .eval index = getCharIndex(c)*4

        .if(index !=0) {
            .byte tiledefs.get(index+2), tiledefs.get(index+3)
        }else{
            .byte tiledefs.get(index+2)
        }
    }
    .byte $fe
}

.function getCharIndex(c) {
    .for(var i=0;i<charbase.size(); i++) {
        .if(c==charbase.charAt(i)){
            .return i
        }
    }
    .error "char not found!"  +c
}
.macro setBitmap(){
    lda $d011
    and #%01110111
    ora #%00100000
    sta $d011
}

.macro setTextMode(){
    lda $d011
    and #%01010111
    sta $d011
}

.macro setMC(){
    lda $d016
    ora #%00010000
    sta $d016
}

.macro setVICBank(bank){
    lda #%11000000 | (3-bank)
    sta $DD00
}

.macro setd018Bitmap(bitmapStart, charColorStart) {
    .var videoMatrix = ((charColorStart & %0011111111111111)/1024)<<4
    .var dotdata = ((bitmapStart &  %0011111111111111)/8192)
    lda #(videoMatrix | dotdata) 
    sta $d018
}
.macro setd018Chars(charDotStart, charsStart) {
    .var videoMatrix = ((charsStart & %0011111111111111)/1024)<<4
    .var dotdata = ((charDotStart &  %0011111111111111)/1024)
    lda #(videoMatrix | dotdata) 
    sta $d018
}


.macro setToText(vicStart, dotStart, charMemAt, rows25){
    .var videoMatrix = ((charMemAt & %0011111111111111)/1024)<<4
    .var dotdata = ((dotStart &  %0011111111111111)/1024)

    lda #(videoMatrix | dotdata) 
    sta $d018

    .var v = $13 //%00000011
    //.eval v= v | %01000000

    lda #v
    sta $d011

    lda $dd02
    ora #3
    sta $dd02
    .var vc = %10010100
    .eval vc = vc | (3-(vicStart/$4000))
    lda #vc 
    lda #$96
    sta $DD00
}
