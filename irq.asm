irq:
    pha
    txa
    pha
    tya
    pha      

    dec lineCount
    jsr getNextRandom    

    lda part
    cmp #1
    beq !+

    jsr morphLogo

    jmp doMusicAndQuit
    //inc $d020
    // setd018Bitmap($4000, CHARS_MEM)
    // setBitmap()
!:
    lda $d011
    and #%01111000
    clc
    adc verticalWobble
    sta $d011
    lda yPos5to7
//    and #%11111000
    clc
    adc verticalWobble
    sta $d00b
    lda yPos5to7+1
//    and #%11111000
    clc
    adc verticalWobble
    sta $d00d
    lda yPos5to7+2
//  and #%11111000
    clc
    adc verticalWobble
    sta $d00f
    //do wobble
    lda wobbleDelay
    clc
    adc #$01
    sta wobbleDelay
    and #%00000111
    bne endWobble

    lda verticalWobble
    eor #$03
    sta verticalWobble

endWobble:
    lda #0
    sta d010
//lazy just loop for speed code
.for(var i=0; i<5; i++) {
    lda counterYSpeed + i
    lsr
    lsr
    lsr
    lsr
    lsr
    tax
    lda spriteSpeedWobble,x
    asl
    clc
    adc spriteSpeedX + i    
    sta toAdd + i
    lda spriteXPos + i*2
    clc
    adc toAdd + i   
    sta spriteXPos + i*2
    bcc !+
    inc spriteXPos +1 + i*2
!:  
speedDone:
    lda spriteXPos + 1 + i*2
    lsr
    lda spriteXPos +i*2
    ror
    sta $d000 + i*2 
    lda spriteXPos+1 + i*2
    lsr
    beq !+
    lda d010
    ora #pow(2,i)
    sta d010
!:  
    lda spriteSpeedY + i
    bpl positiveYSpeed    
    lda spriteYPos + i*2
    clc
    adc spriteSpeedY + i
    sta spriteYPos + i*2
    lda #0
    sta spriteYPos + i*2 + 1
    jmp setPosY
positiveYSpeed:        
    lda spriteYPos + i*2
    clc
    adc spriteSpeedY + i
    sta spriteYPos + i*2
    bcc setPosY
    inc spriteYPos + i*2 + 1
setPosY:  
    lda spriteYPos +i*2 +1
    lsr
    lda spriteYPos +i*2
    ror
    ldx counterYSpeed
    clc
    adc spriteSpeedWobble,x
    sta $d001 +i*2

    lda spriteXPos + 1 + i*2
    cmp #3
    bcc !+
    lda $d015
    and #255-(pow(2,i))
    sta $d015
!:
    inc counterYSpeed + i
//animation
    dec animationDelay
    bne next
    lda #ANIMATION_DELAY
    sta animationDelay
    lda $63f8 + i
    clc
    adc #1

    cmp #CORONA_SPRITE_START+7
    bne !+
    lda #CORONA_SPRITE_START+2
!:  sta $63f8 + i    
next:
}
    lda d010
    sta $d010

aniDone:
//animate hat
    lda hatFrame
    clc
    adc #1
    cmp #14
    bne !+
    lda #0
!:  sta hatFrame 
    tax
    lda hatFrames,x
    sta $63fe
hatDone:
//animate beard
    dec beardFrameCounter
    bne beardDone
    lda #3
    sta beardFrameCounter
    
    lda beardFrame
    clc
    adc #1
    cmp #4
    bne !+
    lda #0
!:  sta beardFrame 
    tax
    lda beardFrames,x
    sta $63ff
beardDone:
//doing ho ho ho or keeping mouth shut
    lda doingTheHo
    beq keepingItShut
//animating ho's
    lax hoFrame
    asl 
    asl     
    asl
    asl
    clc
    adc #$54
    sta $d00a
    lda #$37
    sta yPos5to7
    sta $d00b
    txa
    sec 
    adc #MOUTH_MASK_START
    sta $63fd
    lda #GREEN
    sta $d027+5
    dec hoFrameCounter
    bne doneTheHo
    lda #HO_HO_HO_DELAY
    sta hoFrameCounter
    inx
    stx hoFrame
    cpx #3
    bne doneTheHo
    lda #%11100000
    sta $d015
    ldx #0
    stx hoFrame
    stx doingTheHo
    lda #60
    sta hohohoCounter
keepingItShut:
    lda #$54
    sta $d00a
    lda #$77
    sta yPos5to7
    sta $d00b
    lda #MOUTH_MASK_START
    sta $63fd
    lda #WHITE
    sta $d027+5
    dec hohohoCounter
    bne doneTheHo
    lda #$01
    sta doingTheHo
    lda #255
    sta $d015
    jsr initPlague
doneTheHo:
    lda #220
!:  cmp $d012
    bne !-
    lda $d011
    and #%01110000
    ora #%00001111
    sta $d011

doMusicAndQuit:
    lda #0
    jsr sid.play    
    jsr scroller
    //dec $d020

    lda #$ff 
    sta $d019
    pla
    tay
    pla
    tax
    pla

    rti
initPlague:          

.for(var i=0; i<5; i++) {
    jsr getNextRandom
    lda #$54
    sta $d000+i*2
    asl
    sta spriteXPos + i*2
    lda #0
    sta spriteXPos +i*2 +1
    sta spriteYPos +i*2 +1
    lda #$77
    sta $d001+i*2
    asl
    sta spriteYPos + i*2
    lda #CORONA_SPRITE_START
    sta $63f8+ i
    lax curRandom
    and #$07
    clc
    adc #$04
    sta spriteSpeedX + i
    txa 
    bmi neg
    and #$03
    jmp storeY
neg:  
    and #$03
    eor #$ff
    clc
    adc #$01
storeY:    
    sta spriteSpeedY + i
next:
}
    lda #0
    sta $d010
    sta d010
rts

spriteXPos:
    .byte 0,0,0,0,0,0,0,0,0,0
spriteYPos:
    .byte 0,0,0,0,0,0,0,0,0,0
toAdd:
    .byte 0,0,0,0,0
spriteSpeedX:
    .byte 8,8,8,8,8
spriteSpeedY:
    .byte 0,0,0,0,0
spriteSpeedWobble:
    .fill 256, sin(i*16*3.14/256)*2
counterYSpeed:
    .byte 0,0,0,0,0
outOfView:
    .byte 0,0,0,0,0
hohohoCounter:
    .byte 16        
doingTheHo:
    .byte 0    
yPos5to7:
    .byte 0,0,0    
d010:
    .byte 0    
curRandom:
    .byte (random()*254)+1
    .byte (random()*254)+1
verticalWobble:
    .byte 3    
wobbleDelay:
    .byte 0    